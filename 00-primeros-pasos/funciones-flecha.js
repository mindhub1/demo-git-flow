const x = 10;
const y = 0;

// x + y
var sum = (x, y) => x + y;

// x - y
var subtraction = (x, y) => x - y;

// x / y
var division = (x, y) => x / y;

// validate the divisor is not zero
var validateDivision = (y) => {
    if(y == 0) {
        return false;
    }
    return true;
}

// x * y
var multiplication = (x, y) => x * y;




var main = () => {
    console.log("NUMBERS:", "X =", x, "; Y =", y);
    console.log("SUM:", sum(x, y));
    console.log("SUBTRACTION:", subtraction(x, y));
    if(validateDivision(y)) {
        console.log("DIVISION:", division(x, y));
    } else {
        console.log("DIVISION: No se puede efectuar una division por cero.");
    }
    console.log("MULTIPLICATION:", multiplication(x, y));
}

main();