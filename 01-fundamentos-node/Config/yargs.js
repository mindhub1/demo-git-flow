const argv = require('yargs')(process.argv.slice(2));

const yargsConfig = () => {
	return argv
		.options({
			'b': {
				alias: 'base',
				type: 'number',
				demandOption: true,
				describe: 'Base of the multiplication table'
			},
			'l': {
				alias: 'listar',
				type: 'boolean',
				demandOption: false,
				describe: 'Show the table in cosole',
				default: false
			}
		})
		.check((argv) => {
			const input = argv.b;
			if (isNaN(input)) {
				throw new Error('La base tiene que ser un número');
			} else {
				if(input < 1 || input > 20) {
					throw new Error('Debes colocar un valor de base entre 1 y 20');
				} else {
					return true;
				}
			}
		})
		.argv;
};

module.exports = yargsConfig;