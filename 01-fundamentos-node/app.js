const { argv } = require('yargs');
const createTextFile = require('./modules/createFile');
const yargsConfig = require('./Config/yargs');

const base = argv.b;
const listar = argv.l;


var main = () => {
	yargsConfig();
	createTextFile(base, listar)
		.then(res => console.log(res))
		.catch(err => console.log(err));
};

main();