const fs = require('fs');
const showTableConsole = require('./showTableConsole');


const createTextFile = (base, listar) => {
	return new Promise((resolve, reject) => {
		let title = `----- TABLA DEL ${base} -----`;
		let table = '';
		table += title + '\n';
		for(let i=1; i<=20; i++) {
			let result = base * i;
			let str = `${base} x ${i} = ${result}`;
			table += str + '\n';
		}
		if(listar) {
			showTableConsole(base);
		}

		fs.writeFileSync(`tabla-${base}.txt`, table);

		resolve(`tabla-${base}.txt has been saved!`);
		reject(err => { throw err; });
	});
};

module.exports = createTextFile;