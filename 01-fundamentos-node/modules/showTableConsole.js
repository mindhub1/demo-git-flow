const colors = require('colors');

const starSymbol = '\u2b50 ';

const showTableConsole = (base) => {
	let stars = '';
	let title = `         TABLA DEL ${base}          `;
	if(base.toString().length == 2) {
		title = `         TABLA DEL ${base}         `;
	}
	for(let i=0; i<10; i++) {
		stars += starSymbol;
	}
	console.log(colors.bgBlue(stars));
	console.log(colors.bgBlue(title).bold);
	console.log(colors.bgBlue(stars));

	let table = '';
	let spaces = '          ';
	for(let j=1; j<=20; j++) {
		let str = `${base} x ${j} = ${base*j}`;
		table += spaces + str + spaces + '\n';
	}
	console.log(table.rainbow);
};

module.exports = showTableConsole;